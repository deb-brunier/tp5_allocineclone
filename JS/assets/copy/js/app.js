const INITIAL_SEARCH_VALUE = 'starwars';

// selection des élèments
const searchInput = document.querySelector('#inputSearch');
const searchButton = document.querySelector('#btnSearch');
const moviesSearchable = document.querySelector('#movies-searchable');
const moviesContainer = document.querySelector('#movies-container')
// const imgElement = document.querySelector('img'); 

// imgElement.onclick = function(){
// }



function resquestMovies(url, onComplete, onError){
    // recup les données de TMDB depuis le champ de recherche
    fetch(Url)
        .then((res) => res.json())
        .then(onComplete)
        .catch(onError);

}

function movieSection(movies){
    const section = document.createElement('section');
    section.classList = 'section';

    movies.map((movie) => {
        const img = document.createElement('img');
        img.src = ${IMAGE_URL + movie.poster_path};
        img[data-movie-id] = ${IMAGE_URL + movie.poster_path};
        section appendChild(img);
        }
    })

return section;


// affichage du film
function createMovieContainer(movies, title = '' ){
    const movieElement = document.createMovieElement('div');
    movieElement.setAttribute('class', 'movie');

    const movieTemplate = `
        <h2>${title}</h2>
        <section class="section">
            ${movieSection(movies)}
        </section>
        <div class="content content-display">
            <p id="close">X</p>
        </div>
    `;

    movieElement.innerHTML = movieTemplate;
    return movieElement;
}


function renderSearchMovies(data) {
    movieSearchable.innerHTML = '';
    const movies = data.results;
    const movieBlock = createMovieContainer(movies); 
    movieSearchable.appendChild(movieBlock);
    console.log('Données: ', data);
}

function renderMovies(data) {
    const movies = data.results;
    const movieBlock = createMovieContainer(movies, this.title); 
    movieContainer.appendChild(movieBlock);
}


function searchMovie(value){
    const path = '/search/movie';
    const url = generateUrl(path) + '&query=' + value + '&language=fr';

    requestMovies(url, renderSearchMovies, handleError);
}

function getTopRatedMovies(){
    const path = '/movie/top_rated';
    const url = generateUrl(path);
    const render = renderMovies.bind({ title: 'Films les mieux notés'});
    requestMovies(url, render, handleError);
}

function handleError(error){
    console.log('Erreur: ', error);
}

searchButton.onclick = function(event){
    // empêche le raffraichissement au submit
    event.preventDefault();
    const value = searchInput.value;
    searchMovie(value);

    // recup les données de TMDB depuis une le champ de recherche
    // fetch(newUrl)
    //     .then((res) => res.json())
    //     .then((data) => {
    //         console.log('Données: ', data);
    //     })
    //     .catch((error) => {
    //         console.log('Erreur: ', error);
    //     });

    searchInput.value = '';
    console.log('Valeur: ', value);
}


function createIframe(video){
    const iframe = document.createElement('iframe');
    iframe src = `https://youtube.com/embed/${video_key}`;
    iframe.width = 360;
    iframe.height = 315;
    iframe.allowFullscreen = true;

    return iframe;
}

function createVideoTemplate(data, content){
    // crée <p></p> avant d'executer
    content.innerHTML = '<p id="content-close">X</p>';
    const videos = data.results;
    const lenght = videos.lenght > 4 ? 4 : videos.lenght; 
    const iframeContainer = document.createElement('div');

    for (let i = 0; i < videos.lenght; i++){
        const video = videos[i];
        const iframe = createIframe(video);
        iframeContainer.appendChild(iframe);
        content.appendChild(iframeContainer);
    }
}


// changement de la class en fonction du onclick
document.onclick = function(event) {
    const target = event.target;

    //console.log('Image: ', event);
    // récupère movieId
    const movieId = target.dataset.movieId;

    // si la cible à le tag img
    if (target.tagName.toLowerCase() === 'img') {
        const section = event.target.parentElement;
        const content = section.nextElementSibling;
        // ajoute la class content-display
        content.classList.add('content-display');

        //génère le chemin
        const path = `/movie/${movie_id}/videos`;
        const url = generateUrl(path);

        //appelle l'api
        fetch(url)
        .then((res) => res.json())
        .then((data) => createVideoTemplate(data, content))
        //.then(data) => {
           // console.log('Vidéos: ', data);

        }
        .catch((error) => {
            console.log('Erreur: ', error);
        });
    }

    if (target.id === 'content-close') {
        const content = target.parentElement;
        content.classList.remove('content-display');
    }

}



// Initialize the search
searchMovie(INITIAL_SEARCH_VALUE);


getTopRatedMovies();