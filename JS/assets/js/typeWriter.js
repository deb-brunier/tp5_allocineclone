let app = document.getElementById('typeW');
let typewriter = new Typewriter(app, {loop: true});

typewriter.typeString('AlloCinéClone') //écrit le texte 
    .pauseFor(2500) //pause en millisecondes
    .deleteAll() //reviens en arrière pour supprimer tous les caractères
    .typeString('Tout sur le cinéma') //écrit le texte
    .pauseFor(1000) //pause en millisecondes
    .start(); //recommence au début
